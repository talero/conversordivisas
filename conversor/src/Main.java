import money.MoneyExchage;
import temperature.TemperatureExchange;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        boolean flag = true;
        while (flag) {

            String[] options = {"Conversor de Moneda", "Conversor de Temperatura"};

            ImageIcon convert = new ImageIcon("src/images/convert64.png");

            String selectedValue = (String) JOptionPane.showInputDialog(null,
                    "Seleccion una opcion de conversion", "Menu",
                    JOptionPane.PLAIN_MESSAGE, convert,
                    options, options[0]);

            switch (selectedValue) {
                case "Conversor de Moneda":
                    String inputValue = JOptionPane.showInputDialog("Ingresa la cantidad de dinero que deseas convertir");
                    try {
                        double cashIn = Double.parseDouble(inputValue);
                        MoneyExchage moneyExchage = new MoneyExchage(cashIn);
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "Ingresa un valor numérico", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                    case "Conversor de Temperatura":
                        String inputValue2 = JOptionPane.showInputDialog("Ingresa la cantidad de temperatura que deseas convertir");
                        try {
                            double temperatureIn = Double.parseDouble(inputValue2);
                            TemperatureExchange temperatureExchage = new TemperatureExchange(temperatureIn);
                        } catch (NumberFormatException e) {
                            JOptionPane.showMessageDialog(null, "Ingresa un valor numérico", "ERROR", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
            }

            int response = JOptionPane.showConfirmDialog(null, "¿Desea continuar?", "Conversor de Moneda",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            // 0=yes, 1=no, 2=cancel
            if (response == 1 || response == 2) {
                flag = false;
                JOptionPane.showMessageDialog(null, "Programa Terminado");
            }
        }

    }

}