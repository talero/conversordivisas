package money;

import javax.swing.*;

/**
 * Clase que realiza la conversion de las divisas
 */
public class MoneyExchage {

    double dolar = 4210.23;
    double euro = 4768.90;
    double libra = 4765.67;
    double yen = 3678.89;
    double won = 3358.89;
    String money = "";

    public MoneyExchage(double cashIn) {

        String[] options = {"De Peso a Dolar Estadounidense", "De Peso a Euro", "De Peso a Libras Esterlinas", "De Peso a Yen Japones",
                "De Peso a Won Sur Coreano", "De Dolar Estadounidense a Peso", "De Euro a Peso", "De Libras Esterlinas a Peso",
                "De Yen Japones a Peso", "De Won Sur Coreano a Peso"};

        ImageIcon convert = new ImageIcon("src/images/currency-exchange64.png");
        String option = (String) JOptionPane.showInputDialog(null,
                "Elije la moneda a la que deseas convertir tu dinero", "Monedas",
                JOptionPane.PLAIN_MESSAGE, convert, options, options[0]);

        switch (option) {

            case "De Peso a Dolar Estadounidense":
                money = "Dolares Estadounidense";
                PesoToExchange(cashIn, dolar, money);
                break;
            case "De Peso a Euro":
                money = "Euros";
                PesoToExchange(cashIn, euro, money);
                break;
            case "De Peso a Libras Esterlinas":
                money = "Libras Esterlinas";
                PesoToExchange(cashIn, libra, money);
                break;
            case "De Peso a Yen Japones":
                money = "Yen Japones";
                PesoToExchange(cashIn, yen, money);
                break;
            case "De Peso a Won Sur Coreano":
                money = "Won Sur Coreanos";
                PesoToExchange(cashIn, won, money);
                break;
            case "De Dolar Estadounidense a Peso":
                ExchangeToPesos(dolar, cashIn);
                break;
            case "De Euro a Peso":
                ExchangeToPesos(euro, cashIn);
                break;
            case "De Libras Esterlinas a Peso":
                ExchangeToPesos(libra, cashIn);
                break;
            case "De Yen Japones a Peso":
                ExchangeToPesos(yen, cashIn);
                break;
            case "De Won Sur Coreano a Peso":
                ExchangeToPesos(won, cashIn);
                break;
            default:
                JOptionPane.showMessageDialog(null, "No se ha encontrado la moneda", "ERROR", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }

    /**
     * Metodo encargado de realizar la conversion de Pesos a las divisas y mostrar el resultado
     * @param cashIn Cantidad de dinero base para convertir
     * @param value Valor de la divisa
     * @param money NOmbre de la divisa
     */
    public void PesoToExchange(double cashIn, double value, String money) {

        double cashConverted = Math.round((cashIn / value)*100.0)/100.0;
        JOptionPane.showMessageDialog(null, "Tienes " + cashConverted + " " + money, "RESULTADO", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Metodo encargado de realizar la conversion de las divisas a Pesos y mostrar el resultado
     * @param value Valor de la divisa
     * @param cashIn Cantidad de dinero para convertir
     */
    public void ExchangeToPesos (double value, double cashIn) {

        double cashConverted = Math.round((cashIn * value)*100.0)/100.0;
        JOptionPane.showMessageDialog(null, "Tienes " + cashConverted + " " + "Pesos", "RESULTADO", JOptionPane.PLAIN_MESSAGE);
    }

}

