package temperature;

import javax.swing.*;

/**
 * Clase para la conversion de temperatura
 */
public class TemperatureExchange {

    double temperatureResult = 0;
    String temperatureNameIn = "";
    String temperatureNameOut = "";
    public TemperatureExchange(double temperatureIn){

        String[] options = {"De Grados Celsius a Grados Farenheith", "De Grados Celsius a Grados Kelvin",
                        "De Grados Farenheith a Grados Celsius", "De Grados Farenheith a Grados Kelvin",
                        "De Grados Kelvin a Grados Celsius", "De Grados Kelvin a Grados Farenheith"};

        ImageIcon convert = new ImageIcon("src/images/temperature64.png");
        String option = (String) JOptionPane.showInputDialog(null,
                "Elije una opcion para convertir", "Temperatura",
                JOptionPane.PLAIN_MESSAGE, convert, options, options[0]);

        switch (option){
            case "De Grados Celsius a Grados Farenheith":
                temperatureNameIn = "Grados Celsius";
                temperatureNameOut = "Grados Farenheith";
                temperatureResult = (temperatureIn * 9/5) + 32;
                getTemperature(temperatureNameIn, temperatureNameOut, temperatureResult, temperatureIn);
                break;
            case "De Grados Celsius a Grados Kelvin":
                temperatureNameIn = "Grados Celsius";
                temperatureNameOut = "Grados Kelvin";
                temperatureResult = temperatureIn + 273.15;
                getTemperature(temperatureNameIn, temperatureNameOut, temperatureResult, temperatureIn);
                break;
            case "De Grados Farenheith a Grados Celsius":
                temperatureNameIn = "Grados Farenheith";
                temperatureNameOut = "Grados Celsius";
                temperatureResult = (temperatureIn - 32) * 5/9;
                getTemperature(temperatureNameIn, temperatureNameOut, temperatureResult, temperatureIn);
                break;
            case "De Grados Farenheith a Grados Kelvin":
                temperatureNameIn = "Grados Farenheith";
                temperatureNameOut = "Grados Kelvin";
                temperatureResult = (temperatureIn + 459.67) * 5/9;
                getTemperature(temperatureNameIn, temperatureNameOut, temperatureResult, temperatureIn);
                break;
            case "De Grados Kelvin a Grados Celsius":
                temperatureNameIn = "Grados Kelvin";
                temperatureNameOut = "Grados Celsius";
                temperatureResult = temperatureIn - 273.15;
                getTemperature(temperatureNameIn, temperatureNameOut, temperatureResult, temperatureIn);
                break;
            case "De Grados Kelvin a Grados Farenheith":
                temperatureNameIn = "Grados Kelvin";
                temperatureNameOut = "Grados Farenheith";
                temperatureResult = (temperatureIn * 9/5) - 459.67;
                getTemperature(temperatureNameIn, temperatureNameOut, temperatureResult, temperatureIn);
                break;
        }
    }

    public void getTemperature(String temperatureNameIn, String temperatureNameOut, double temperatureResult, double temperatureIn){
        JOptionPane.showMessageDialog(null, temperatureIn + " " + temperatureNameIn + " equivalen a " + temperatureResult + " " + temperatureNameOut, "RESULTADO", JOptionPane.PLAIN_MESSAGE);
    }
}
